extends "res://Scenes/Guns/Bullets/BulletBase.gd"

export(int) var response_velocity

func _process(delta):
    var gmp = get_global_mouse_position()
    impulse += (gmp - get_global_position()).normalized() * 2
    look_at(impulse * 1000.0)
    ._process(delta)