extends KinematicBody2D

export(int) var damage
export(float) var die_time
export(SpriteFrames) var hit_animation
export(SpriteFrames) var wall_hit_animation
export(bool) var continue_after_enemy_hit
export(String) var attack_group

var impulse

func _ready():
    z_index = 500
    var timer = Timer.new()
    add_child(timer)
    timer.connect("timeout", self, "queue_free")
    timer.set_wait_time(die_time)
    timer.start()

func _process(delta):
    var event = move_and_collide(impulse * delta)
    if event:
        var do_hit_anim = false
        var do_wall_anim = false
        var colliding_body = event.collider

        if hit_animation and wall_hit_animation:
            if colliding_body.is_in_group("enemy") or colliding_body.is_in_group("destructable"):
                do_hit_anim = true
            else:
                do_wall_anim = true
        elif hit_animation:
            do_hit_anim = true

        var animation
        if do_hit_anim:
            animation = hit_animation
        elif do_wall_anim:
            animation = wall_hit_animation

        if animation:
            var anim = global.animated_explosion.instance()
            anim.get_node("AnimatedSprite").set_sprite_frames(animation)
            anim.set_global_position(get_global_position())
            anim.get_node("AnimatedSprite").play()
            get_parent().add_child(anim)

        if colliding_body.is_in_group(attack_group):
            colliding_body.get_node("health").get_hit(damage)
            colliding_body.apply_impulse(impulse * 0.1)
        elif colliding_body.is_in_group("destructable") and colliding_body.get_node("health"):
            colliding_body.get_node("health").get_hit(damage)

        if not continue_after_enemy_hit:
            queue_free()

func apply_impulse(nope, i):
    impulse = i
