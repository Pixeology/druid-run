extends AnimatedSprite

export(String) var projectile_name
export(int) var projectile_amount
export(int) var projectile_speed
export(float) var muzzle_spread
export(float) var accuracy
export(int) var recoil
export(int) var camera_shake
export(int) var starting_ammo
export(int) var ammo_received_per_pack

var player_ref
var dropped = false
onready var gun_name = get_parent().get_name()

func _ready():
    self.connect("animation_finished", self, "spawn_bullets")

func drop():
    dropped = true
    print("Dropped!")
    z_index = global.calculate_z_index(get_global_position())

func _process(delta):
    if not player_ref:
        player_ref = get_tree().get_root().get_node("Loader/NavigationLayer/Player")
    var gmp = get_global_mouse_position()
    if $"/root/Loader".game_mode == $"/root/Loader".GameMode.BUILD:
        self.visible = false
    if not dropped and $"/root/Loader".game_mode != $"/root/Loader".GameMode.BUILD:
        self.visible = true
        look_at(gmp)
        rotation_degrees += 45
        if Input.is_mouse_button_pressed(BUTTON_LEFT) and not self.is_playing() and not player_ref.running:
            self.play("Fire")

func do_effects(gmp, start):
    if not dropped:
        player_ref.apply_impulse(-(gmp - start).normalized() * recoil)
        player_ref.get_node("Camera2D").camera_shake(camera_shake, 0.3)

func spawn_bullets():
    self.stop()
    var gmp = get_global_mouse_position()
    var start = player_ref.get_global_position()
    do_effects(gmp, start)

    global.gun_ammos[gun_name] -= projectile_amount
    var start_dir = (gmp - start).normalized()
    var rot_dir = muzzle_spread / projectile_amount
    if projectile_amount > 1:
        var amount = projectile_amount / 2
        start_dir = start_dir.rotated(deg2rad(-rot_dir * amount))
    for i in range(0, projectile_amount):
        var misfire = deg2rad(rand_range(-accuracy/2, accuracy/2))
        var direction = start_dir.rotated(misfire)
        look_at((gmp - start).normalized().rotated(misfire) * 50 + start)
        var barrel_length = 60
        spawn_new_bullet(start, direction, barrel_length)
        start_dir = start_dir.rotated(deg2rad(rot_dir))

func spawn_new_bullet(start, direction, barrel_length):
    if not dropped:
        var bullet = load("res://Scenes/Guns/Bullets/" + projectile_name + ".tscn").instance()
        bullet.look_at(direction * projectile_speed)
        get_tree().get_root().get_node("Loader").add_child(bullet)
        bullet.set_global_position(start + direction * barrel_length)
        bullet.apply_impulse(Vector2(0,0), direction * projectile_speed)

func more_ammo():
    global.gun_ammos[gun_name] += ammo_received_per_pack
