extends Node2D

export(int) var spawn_radius
var goal_pos_anim
onready var animated = false
export(SpriteFrames) var drop_animation
export(int) var number_of_items_to_spawn
var spawned_items = false

func _ready():
    goal_pos_anim = get_global_position()
    $CollisionShape2D.disabled = true
    set_global_position(goal_pos_anim - Vector2(0, 800))

func _process(delta):
    if get_global_position().distance_to(goal_pos_anim) > 20.0:
        var diff = (goal_pos_anim - get_global_position()).normalized()
        set_global_position(get_global_position() + diff * delta * 300)
    if get_global_position().distance_to(goal_pos_anim) <= 20.0:
        $CollisionShape2D.disabled = false
        if not animated:
            animated = true
            if drop_animation:
                var anim = global.animated_explosion.instance()
                anim.get_node("AnimatedSprite").set_sprite_frames(drop_animation)
                anim.set_global_position(get_global_position() + Vector2(0, $AnimatedSprite.frames.get_frame($AnimatedSprite.animation, $AnimatedSprite.frame).get_height()))
                anim.get_node("AnimatedSprite").play()
                get_parent().add_child(anim)

func die():
    print("Crate destroyed!")
    var items = global.all_items_for_level(global.player_level)

    $AnimatedSprite.play()
    yield($AnimatedSprite, "animation_finished")
    if not spawned_items:
        choose_items(items)
        spawned_items = true
    queue_free()

func choose_items(items):
    var to_make_items
    for i in range(0, number_of_items_to_spawn):
        var e = items[randi() % items.size()].instance()
        if not e.get_name() in global.player_gun_selection_names:
            var rand_pos = Vector2((randi() % (2*spawn_radius) - spawn_radius) + 16*4, (randi() % (2*spawn_radius) - spawn_radius) + 16*4)
            e.set_global_position(self.get_global_position() + rand_pos)
            if e.is_in_group("weapon"):
                e.get_node("Sprite").dropped = true
            else:
                e.dropped = true
            get_parent().add_child(e)
