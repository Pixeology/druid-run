extends Area2D

export(int) var pair_number
export(int) var teleport_delay
export(String) var id
onready var last_teleport_time = OS.get_ticks_msec()
var dropped

func _ready():
    z_index = global.calculate_z_index(get_global_position())

func _process(delta):
    if not dropped:
        if not $AnimatedSprite.playing and OS.get_ticks_msec() - last_teleport_time > teleport_delay:
            $AnimatedSprite.play()
        elif $AnimatedSprite.playing and OS.get_ticks_msec() - last_teleport_time <= teleport_delay:
            $AnimatedSprite.stop()

func _on_TeleporterBlue_body_entered(body):
    if not dropped and body.is_in_group("living"):
        if OS.get_ticks_msec() - last_teleport_time > teleport_delay:
            var targets = get_tree().get_nodes_in_group("red" if id == "blue" else "blue")
            if targets.size() > pair_number:
                var target = targets[pair_number]
                target.last_teleport_time = OS.get_ticks_msec()
                body.set_global_position(target.get_global_position()) #  + body.dir * 32 * 4)
                last_teleport_time = OS.get_ticks_msec()
    elif not dropped and body.is_in_group("bullet"):
        var targets = get_tree().get_nodes_in_group("red" if id == "blue" else "blue")
        if targets.size() > pair_number:
            var target = targets[pair_number]
            body.set_global_position(target.get_global_position()) #  + body.impulse.normalized() * 32 * 4