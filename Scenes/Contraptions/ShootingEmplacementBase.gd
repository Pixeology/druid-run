extends Node2D

var timer
export(float) var shot_delay
export(int) var projectile_speed
export(String) var projectile_name
export(bool) var aim_at_nearest_enemy
var dir
var dropped

func _ready():
    if not dropped:
        dir = global.callv("get_animation_and_direction", global.get_flip_flags_from_animation($AnimatedSprite.animation))[1]

        timer = Timer.new()
        add_child(timer)

        timer.connect("timeout", self, "_Timer_shoot_time")
        timer.set_wait_time(shot_delay)
        timer.set_one_shot(false)
        timer.start()
    z_index = global.calculate_z_index(get_global_position())

func spawn_new_bullet(start, direction, barrel_length):
    $AnimatedSprite.stop()
    var ll = get_tree().get_root().get_node("Loader")
    if ll.game_mode == ll.GameMode.DEFEND:
        var arrow = load("res://Scenes/Guns/Bullets/"+projectile_name+".tscn").instance()
        arrow.look_at(direction * projectile_speed)
        arrow.rotation_degrees -= 90
        ll.add_child(arrow)
        arrow.set_global_position(start + direction * barrel_length)
        arrow.apply_impulse(Vector2(0,0), direction * projectile_speed)

func _Timer_shoot_time():
    if not dropped:
        var direction = dir
        if aim_at_nearest_enemy:
            var min_dist = INF
            var enemy
            for c in get_parent().get_children():
                var d = c.get_global_position().distance_to(self.get_global_position()) < min_dist
                if c.is_in_group("enemy") and d:
                    enemy = c
                    min_dist = d
            if enemy:
                direction = (enemy.get_global_position() - self.get_global_position()).normalized()
        $AnimatedSprite.play()
        $AnimatedSprite.connect("animation_finished", self, "spawn_new_bullet", [self.get_position(), direction, 16*4])
