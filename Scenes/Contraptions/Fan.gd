extends Node2D

var direction

export(int) var fan_strength
export(int) var fan_throw

var enemies = []
var enemy_count = {}
var dir
var dropped

func _ready():
    dir = global.callv("get_animation_and_direction", global.get_flip_flags_from_animation($AnimatedSprite.animation))[1]
    var node = $AirAnimation
    node.set_position(dir * 24 * 4)
    node.look_at(self.global_position - dir * 100)
    if dir.y == -1:
        node.show_behind_parent = true
    node.play()
    z_index = global.calculate_z_index(get_global_position())

func _physics_process(delta):
    if not dropped:
        var bodies = $AirAnimation/Air.get_overlapping_bodies()
        for b in bodies:
            if not enemies.has(b.get_name()) and b.is_in_group("enemy"):
                b.apply_impulse(dir * fan_strength)

func _on_Area2D_body_entered(body):
    if not dropped:
        if body.is_in_group("enemy") and not enemies.has(body.get_name()) and not enemy_count.has(body.get_name()):
            body.apply_impulse(dir * fan_throw)
            body.stun(3)

func _on_Area2D_body_exited(body):
    if not dropped:
        if body.is_in_group("enemy"):
            var n = body.get_name()
            if not enemy_count.has(n):
                enemy_count[n] = 1
            elif enemy_count[n] > 1 and not enemies.has(n):
                enemies.append(n)
            elif enemy_count[n] <= 1:
                enemy_count[n] += 1