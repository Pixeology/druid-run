extends Area2D

export(float) var stun_time
var enemies = []
var dropped

func _ready():
    z_index = global.calculate_z_index(get_global_position())

func _on_Tripwire_body_entered(body):
    if not dropped:
        var n = body.get_name()
        if body.is_in_group("enemy") and not enemies.has(n):
            body.stun(stun_time)
            enemies.append(n)
