extends Label

export(int) var show_time
var show_message_time

func show_message(text):
    self.text = text
    show_message_time = OS.get_ticks_msec()
    self.visible = true

func _process(delta):
    if self.visible and OS.get_ticks_msec() - show_message_time > show_time:
        self.visible = false