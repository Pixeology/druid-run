extends Node2D

func _ready():
    set_process(true)

func _process(delta):
    update()

func _draw():
    var player = get_parent().get_node("Player")
    if player:
        var cp = player.get_node("Camera2D").get_camera_position()
        draw_line(player.get_global_position() - cp, get_viewport().get_mouse_position(), Color(255, 255, 255), 10)