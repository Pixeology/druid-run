extends Node2D

onready var player = preload("res://Actors/Player.tscn")
onready var enemy = preload("res://Actors/Enemy.tscn")
onready var bomb_skele = preload("res://Actors/BombSkeleton.tscn")
onready var elf = preload("res://Actors/Elf.tscn")
onready var blaze = preload("res://Actors/Blaze.tscn")
onready var bastion = preload("res://Actors/Bastion.tscn")
onready var loot_crate = preload("res://Scenes/LootCrate.tscn")
onready var last_wave_time = OS.get_ticks_msec()
export(int) var wave_interval
export(Array, String) var level_names = ["The Death Of MurderGodot"]
export(int) var build_time
export(Texture) var building_arrow
export(int) var waves_per_level

enum GameMode {
    BUILD,
    DEFEND,
    GET_LOOT
   }
onready var game_mode = GameMode.BUILD
var cursor_texture
var shot_cursor_texture
var selected_emp
var selected_emp_i
var textures
var emp_amounts = {}
var wave_count = 0
var stop_build = false

func _ready():
    var img = Image.new()
    img.load("res://Assets/Tiles/Cursor.png")
    cursor_texture = ImageTexture.new()
    cursor_texture.create_from_image(img)

    img = Image.new()
    img.load("res://Assets/Tiles/ShotCursor.png")
    shot_cursor_texture = ImageTexture.new()
    shot_cursor_texture.create_from_image(img)

    load_level(get_node("/root/global").player_level)
    set_process(true)
    global.reload_default_guns()

func spawn_actors(player_spawned = false, bastion_spawned = false, no_enemies = true):
    for tmx_layer in $NavigationLayer.get_children().front().get_children():
        if tmx_layer.get_class() == "Node2D" && tmx_layer.get_name() == "Spawnlayer":
            for spawn_point in tmx_layer.get_children():
                var entity
                if "enemy" in spawn_point.get_name() and "spawn" in spawn_point.get_name() and not no_enemies:
                    entity = enemy.instance()
                    entity.stop = true
                elif "player" in spawn_point.get_name() and "spawn" in spawn_point.get_name() and not player_spawned:
                    entity = player.instance()
                    player_spawned = true
                elif "bastion" in spawn_point.get_name() and "spawn" in spawn_point.get_name() and not bastion_spawned:
                    entity = bastion.instance()
                    bastion_spawned = true
                elif "bomb" in spawn_point.get_name() and "spawn" in spawn_point.get_name() and not no_enemies:
                    entity = bomb_skele.instance()
                    entity.stop = true
                elif "elf" in spawn_point.get_name() and "spawn" in spawn_point.get_name() and not no_enemies:
                    entity = elf.instance()
                    entity.stop = true
                elif "blaze" in spawn_point.get_name() and "spawn" in spawn_point.get_name() and not no_enemies:
                    entity = blaze.instance()
                    entity.stop = true

                if not entity:
                    print(spawn_point.get_name())
                else:
                    entity.set_global_position(spawn_point.get_global_position())
                    entity.z_index = 100
                    $NavigationLayer.add_child(entity)

func switch_level(l):
    wave_count = 0
    for c in get_children():
        if not c.is_in_group("loader"):
            remove_child(c)
    for c in $NavigationLayer.get_children():
        if not c.is_in_group("loader"):
            $NavigationLayer.remove_child(c)
    load_level(l)

func load_level(l):
    print("loading level " + level_names[l - 1])

    var name = "res://Scenes/" + level_names[l - 1] + ".tscn"
    var scene = load(name).instance()
    scene.scale = Vector2(4, 4)
    scene.z_index = 0
    $NavigationLayer.add_child(scene)

    spawn_actors()

    $HUD/Message.show_message("Level " + str(l) + ".\nPrepare your defenses!")

    emp_amounts = global.player_emp_selection_limits.duplicate()
    start_build(false)
    last_wave_time = OS.get_ticks_msec()

    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _process(delta):
    if Input.is_action_just_pressed("ui_end") and game_mode == GameMode.BUILD:
        start_defend()
        last_wave_time = OS.get_ticks_msec()
    elif Input.is_action_just_pressed("ui_end") and game_mode == GameMode.GET_LOOT:
        global.player_level += 1
        switch_level(global.player_level)
    var player = $NavigationLayer.get_node("Player")
    var health = player.get_node("health")
    var bastion = $NavigationLayer.get_node("Bastion/health")
    var gname = player.current_gun.get_name()
    $HUD/HUDOrganizer/PlayerStats.text = "Health: " + str(health.health) + ", Bastion: " + str(bastion.health)
    $HUD/HUDOrganizer/Equipped.text = "Loaded: " + str(gname)
    $HUD/HUDOrganizer/AmmoCounter.text = "Ammo: " + str(global.gun_ammos[gname])
    if global.player_gun_selection_names.size() > 1:
        $HUD/HUDOrganizer/Alternate.text = "Alternate: " + str(global.player_gun_selection_names[int(!player.current_gun_alt)])
    else:
        $HUD/HUDOrganizer/Alternate.text = "[No Alternate]"
    if game_mode == GameMode.DEFEND and (OS.get_ticks_msec() - last_wave_time > wave_interval):
        if wave_count < waves_per_level:
            if no_enemies():
                start_build()
                last_wave_time = OS.get_ticks_msec()
        elif no_enemies() and wave_count >= waves_per_level:
            start_loot_phase()
    elif game_mode == GameMode.BUILD and OS.get_ticks_msec() - last_wave_time > build_time:
        start_defend()
        last_wave_time = OS.get_ticks_msec()
    update()

func no_enemies():
    for c in $NavigationLayer.get_children():
        if c.is_in_group("enemy"):
            return false
    return true

func start_loot_phase():
    game_mode = GameMode.GET_LOOT
    $HUD/Message.show_message("The Loot Crate has Landed!")
    for tmx_layer in $NavigationLayer.get_children().front().get_children():
        if tmx_layer.get_class() == "Node2D" && tmx_layer.get_name() == "Spawnlayer":
            for spawn_point in tmx_layer.get_children():
                if "loot" in spawn_point.get_name():
                    var entity = loot_crate.instance()
                    entity.set_global_position(spawn_point.get_global_position())
                    entity.z_index = 100
                    $NavigationLayer.add_child(entity)

func start_build(show_message=true):
    game_mode = GameMode.BUILD
    if show_message: $HUD/Message.show_message("Prepare your defenses!")
    $HUD/BuildBar.visible = true
    populate_buildbar()
    wave_count += 1

func populate_buildbar():
    textures = []
    global.load_available_emps()
    for c in $HUD/BuildBar.get_children():
        $HUD/BuildBar.remove_child(c)
    for i in range(global.player_emp_selection.size()):
        var emp = global.player_emp_selection[i].instance()

        var button = generate_button_from_emplacement(emp, i)
        $HUD/BuildBar.add_child(button)

func generate_button_from_emplacement(emp, i):
    var sprites = emp.get_node("AnimatedSprite")
    var texture_1
    if sprites:
        texture_1 = sprites.frames.get_frame("Horizontal", 0)
        textures.append([
            texture_1,
            sprites.frames.get_frame("Horizontal_Right", 0),
            sprites.frames.get_frame("Vertical", 0),
            sprites.frames.get_frame("Vertical_Down", 0)
        ])
    else:
        sprites = emp.get_node("Sprite")
        texture_1 = sprites.texture
        textures.append([
            sprites.texture,
            sprites.texture,
            sprites.texture,
            sprites.texture
        ])

    var button = Button.new()

    button.connect("button_down", self, "_emp_button_click", [emp.get_name(), i])
    button.connect("mouse_entered", self, "_on_BuildBar_mouse_entered")
    button.connect("mouse_exited", self, "_on_BuildBar_mouse_exited")

    button.name = "EmpButton" + str(i)
    button.size_flags_horizontal = Button.SIZE_EXPAND_FILL
    button.size_flags_vertical = Button.SIZE_EXPAND_FILL
    button.flat = true
    button.text = str(emp_amounts[global.player_emp_selection_names[i]])

    button.add_font_override("font", load("res://FixedsysActual32.tres"))

    var rect = TextureRect.new()
    rect.set_scale(Vector2(4,4))
    rect.set_texture(texture_1)

    button.add_child(rect)
    return button

func start_defend():
    stop_build = false
    game_mode = GameMode.DEFEND
    $HUD/Message.show_message("Defend the Bastion!")
    spawn_actors(true, true, false)
    $HUD/BuildBar.visible = false

func calculate_dir(b, a):
    var dx = a.x - b.x
    var dy = a.y - a.y
    return Vector2(dx / abs(dx) if abs(dx) != 0 else 0,
                   dy / abs(dy) if abs(dy) != 0 else 0)

var mouse_ray_result = Dictionary()
func _physics_process(delta):
    var player = $NavigationLayer.get_node("Player")
    var space_state = get_world_2d().direct_space_state
 # use global coordinates, not local to node
    var gmp = get_global_mouse_position()
    var start = player.get_global_position() + Vector2(12, 10)

    var dir = calculate_dir(start, gmp)
    start.x += dir.x * 30
    start.y += dir.y * 25

    mouse_ray_result = space_state.intersect_ray(start, gmp, [], player.collision_mask)

func _draw():
    var player = $NavigationLayer.get_node("Player")
    if Input.get_mouse_mode() == Input.MOUSE_MODE_HIDDEN:
        var pos = get_local_mouse_position() - Vector2(40,40)
        if game_mode == GameMode.BUILD:
            pos = Vector2(floor(pos.x / (16*4)), floor(pos.y / (16*4))) * (16*4)
            if selected_emp:
                var rect = Rect2(pos, Vector2(16*4, 16*4))

                var res = global.get_animation_and_direction(player.build_vertical, player.build_flip)
                var frame = res[0]
                var direction = res[1]

                var t = textures[selected_emp_i][frame]
                if not t:
                    t = textures[selected_emp_i][int(player.build_vertical)*2]
                draw_texture_rect(t, rect, false, Color(1, 1, 1, 0.5))

                var rect2 = Rect2(pos + direction * 16 * 4, Vector2(16*4, 16*4))
                draw_texture_rect(building_arrow, rect2, false, Color(1, 1, 1, 1))
            draw_texture(cursor_texture, pos)
        else:
            draw_texture(shot_cursor_texture, pos)

func _on_BuildBar_focus_entered():
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_BuildBar_focus_exited():
    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)


func _on_BuildBar_mouse_entered():
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func _on_BuildBar_mouse_exited():
    Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _emp_button_click(name, i):
    selected_emp = global.player_emp_selection[i]
    selected_emp_i = i
