extends "res://Actors/Enemy.gd"

export(String) var projectile_name
export(int) var projectile_speed
export(int) var barrel_length
export(int) var projectile_amount
export(int) var muzzle_spread
export(int) var accuracy
export(int) var hang_back_dist
export(int) var shot_delay

onready var last_shot_time = OS.get_ticks_msec()
var space_state
var can_see_player
var ray_result

func _ready():
    goal_dist = hang_back_dist


func _select_player_ref():
    return get_parent().get_node("Player")

func _physics_process(delta):
    ._physics_process(delta)

    if not space_state:
        space_state = get_world_2d().direct_space_state

    var start = get_global_position()
    var dir = (player_ref.get_global_position() - start).normalized()
    ray_result = space_state.intersect_ray(start, start + dir * 500 * 4, [self])
    can_see_player = not ray_result.empty() and ray_result.collider and ray_result.collider.is_in_group("player")

func spawn_new_bullet(start, direction, barrel_length):
    var bullet = load("res://Scenes/Guns/Bullets/" + projectile_name + ".tscn").instance()
    get_tree().get_root().get_node("Loader").add_child(bullet)
    bullet.look_at(direction * projectile_speed)
    bullet.set_global_position(start + direction * barrel_length)
    bullet.apply_impulse(Vector2(0,0), direction * projectile_speed)

func _arrived_at_point():
    if not self.stop and not self.stunned and can_see_player and OS.get_ticks_msec() - last_shot_time > shot_delay:
        var gmp = player_ref.get_global_position()
        var start = self.get_global_position()

        var start_dir = (gmp - start).normalized()
        var rot_dir = muzzle_spread / projectile_amount
        if projectile_amount > 1:
            var amount = projectile_amount / 2
            start_dir = start_dir.rotated(deg2rad(-rot_dir * amount))
        for i in range(0, projectile_amount):
            var misfire = deg2rad(rand_range(-accuracy/2, accuracy/2))
            var direction = start_dir.rotated(misfire)
            var barrel_length = 60
            spawn_new_bullet(start, direction, barrel_length)
            start_dir = start_dir.rotated(deg2rad(rot_dir))
        last_shot_time = OS.get_ticks_msec()