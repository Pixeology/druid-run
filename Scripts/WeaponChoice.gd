extends Control

var current_selection = 0

func list_files_in_directory(path):
    var files = []
    var dir = Directory.new()
    dir.open(path)
    dir.list_dir_begin()

    while true:
        var file = dir.get_next()
        if file == "":
            break
        elif not file.begins_with(".") and file.ends_with("tscn"):
            files.append(file)

    dir.list_dir_end()

    return files

func _ready():
    var weapons = list_files_in_directory("res://Scenes/Guns")
    for weapon in weapons:
        var name = weapon.replace(".tscn", "")
        var button = Button.new()
        button.flat = true
        button.text = name
        button.add_font_override("font", load("res://Fixedsys32.tres"))
        button.connect("pressed", self, "weapon_selected", [name])
        button.add_color_override("font_color", Color(0,0,0))
        $CenterRow/WeaponSelection/VBoxContainer.add_child(button)

func _on_Button_pressed():
    current_selection = 0
    $CenterRow/Buttons/Button2.pressed = false

func _on_Button2_pressed():
    current_selection = 1
    $CenterRow/Buttons/Button.pressed = false

func weapon_selected(name):
    print(global.player_gun_selection_names)
    global.player_gun_selection_names[current_selection] = name