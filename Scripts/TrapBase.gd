extends Area2D

export(int) var cooldown_time
export(float) var stun_time_secs
export(int) var damage
onready var last_trap_time = OS.get_ticks_msec()
var dropped

func _on_Trap_body_entered(body):
    if not dropped:
        if OS.get_ticks_msec() - last_trap_time >= cooldown_time:
            if body.is_in_group("enemy"):
                $AnimatedSprite.play()
                body.stun(stun_time_secs)
                body.get_node("health").get_hit(damage)
                yield($AnimatedSprite, "animation_finished")
                $AnimatedSprite.stop()
                $AnimatedSprite.frame = 0
                last_trap_time = OS.get_ticks_msec()