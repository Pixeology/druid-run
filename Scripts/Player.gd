extends KinematicBody2D

export(int) var velocity
export(float) var acceleration
export(float) var deceleration
export(float) var health_improvement

onready var tilemap2 = get_parent().get_child(0).get_node("Tiles")
onready var tilemap1 = get_parent().get_child(0).get_node("Dirt")
onready var time_last_drop = OS.get_ticks_msec()

var current_gun
var current_gun_alt = false
var running = false
var build_vertical = false
var build_flip = false
var dir
var regex
var current_speed = Vector2(0, 0)

func _ready():
    regex = RegEx.new()
    regex.compile("[@\\d]")
    print("Ready called")
    update_gun()

func update_gun():
    var gun_pre = global.player_gun_selection[int(current_gun_alt) % global.player_gun_selection.size()]
    current_gun = gun_pre.instance()
    for c in $Front_Arm/GunPosition.get_children():
        if c.is_in_group("weapon"):
            $Front_Arm/GunPosition.remove_child(c)
    current_gun.show_behind_parent = true
    if not current_gun.is_in_group("gun"):
        current_gun.set_position(Vector2(8, 0))
    $Front_Arm/GunPosition.add_child(current_gun)

func isneg(x, y):
    return x != 0 and y != 0 and x/abs(x) == -(y/abs(y))

func handle_basic_movement(delta, gmp):
    running = Input.is_mouse_button_pressed(BUTTON_RIGHT)
    if running:
        $Sprite.frames.set_animation_speed("Running", 15)
        current_speed *= 1.4
    else:
        $Sprite.frames.set_animation_speed("Running", 10)

    var goal_speed = Vector2(
        velocity * (-int(Input.is_key_pressed(KEY_A)) + int(Input.is_key_pressed(KEY_D))),
        velocity * (-int(Input.is_key_pressed(KEY_W)) + int(Input.is_key_pressed(KEY_S)))
    )
    var multx = acceleration
    var multy = acceleration
    if goal_speed.x == 0 or isneg(goal_speed.x, current_speed.x):
        multx = deceleration
    if goal_speed.y == 0 or isneg(goal_speed.y, current_speed.y):
        multy = deceleration
    current_speed.x = lerp(current_speed.x, goal_speed.x, multx * delta)
    current_speed.y = lerp(current_speed.y, goal_speed.y, multy * delta)

    var npos = current_speed * delta + get_global_position()
    var map_pos = Vector2(floor(npos.x / (16*4)), floor(npos.y / (16*4)))
    var t = tilemap2.get_cell(map_pos.x, map_pos.y+1)
    if t >= 34 and t <= 39:
        current_speed = 0

    var event = move_and_collide(current_speed)
    if event:
        current_speed = Vector2(0,0)

func apply_impulse(vec):
    current_speed += vec

func update_player_animations(gmp):
    # Handle animations
    if gmp.x < get_global_position().x:
        $Sprite.flip_h = true
    elif gmp.x > get_global_position().x:
        $Sprite.flip_h = false

    if current_speed.x != 0 or current_speed.y != 0:
        $Sprite.animation = "Running"
    else:
        $Sprite.animation = "default"

func handle_build_mode_inputs(delta, dir, gmp, ll):
    if Input.is_action_just_released("ui_build_emp") and get_viewport().get_mouse_position().y < 1009:
        if ll.selected_emp and ll.emp_amounts[global.player_emp_selection_names[ll.selected_emp_i]] > 0:
            var emp = ll.selected_emp.instance()

            var animations = emp.get_node("AnimatedSprite").frames.get_animation_names()
            var res = global.get_animation_and_direction(build_vertical, build_flip)
            emp.get_node("AnimatedSprite").animation = animations[res[0] % animations.size()]

            var pos = get_global_mouse_position() - Vector2(40,40)
            var map_pos = Vector2(floor(pos.x / (16*4)),
                                  floor(pos.y / (16*4)))

            var tile_id1 = tilemap1.get_cell(map_pos.x, map_pos.y)
            var tile_id2 = tilemap2.get_cell(map_pos.x, map_pos.y)
            var posp = (map_pos + Vector2(0.5, 0.5)) * (16*4)
            emp.set_global_position(posp)
            if emp.get_node("CollisionShape2D") and emp.get_class() == "StaticBody2D":
                emp.prev_id = [tile_id1, tile_id2]
                tilemap1.set_cell(map_pos.x, map_pos.y, -1)
                tilemap2.set_cell(map_pos.x, map_pos.y, -1)
            if not global.get_node_at_pos(posp):
                ll.emp_amounts[global.player_emp_selection_names[ll.selected_emp_i]] -= 1
                get_parent().add_child(emp)
    if Input.is_action_just_pressed("ui_change_gun"):
        rotate_build_flags_left()
    if Input.is_action_just_pressed("ui_drop_gun"):
        rotate_build_flags_right()

func handle_defend_mode_inputs(delta, dir, gmp):
    if Input.is_action_just_pressed("ui_change_gun"):
        current_gun_alt = !current_gun_alt
        update_gun()

    if Input.is_action_just_pressed("ui_build_emp"):
        origional_rotation = $Front_Arm.rotation_degrees

    if Input.is_action_just_pressed("ui_drop_gun") \
        and global.player_gun_selection.size() > 1:
        current_gun_alt = !current_gun_alt

        remove_child(current_gun)
        print("Removed: " + current_gun.get_name())

        # F&!* Godot
        var dup = current_gun.duplicate()
        dup.get_node("Sprite").dropped = true
        dup.show_behind_parent = false
        dup.set_global_position(self.get_global_position() + Vector2(29,29))
        get_parent().add_child(dup)
        current_gun.queue_free()

        global.player_gun_selection_names.erase(current_gun.get_name())
        global.load_available_guns()
        update_gun()

        time_last_drop = OS.get_ticks_msec()


var origional_rotation = 0

func update_arms(gmp):
    var fc = -1 if not $Sprite.flip_h else 1
    var foffset = 4 * $Sprite.frame if $Sprite.animation == "default" else 0

    $Rear_Arm.set_position(Vector2(14.146 * fc, -8 + foffset))
    $Rear_Arm.set_scale(Vector2(4, -4 * fc))
    $Rear_Arm.look_at(gmp)
    $Rear_Arm.rotation_degrees += 45 * fc

    $ShoulderPlate.animation = $Sprite.animation
    $ShoulderPlate.flip_h = $Sprite.flip_h
    $ShoulderPlate.frame = $Sprite.frame

    var shoulder_offset = (-abs($ShoulderPlate.frame / 2 - 2) * 4) if $ShoulderPlate.animation == "Running" else 0
    $Front_Arm.set_position(Vector2((14.146 + shoulder_offset) * fc, -12.101 + foffset))
    $Front_Arm.set_scale(Vector2(4, -4 * fc))
    $Front_Arm.look_at(gmp)
    $Front_Arm.rotation_degrees += 45 * fc

    var weapon = $Front_Arm/GunPosition.get_child(0)
    if not weapon.is_in_group("gun"):
        if weapon.get_node("Sprite").frame == 0 and weapon.get_node("AnimationPlayer").is_playing():
            $Front_Arm.rotation_degrees = origional_rotation - 180
        elif weapon.get_node("AnimationPlayer").is_playing():
            $Front_Arm.rotation_degrees = origional_rotation

func _physics_process(delta):
    var gmp = get_global_mouse_position()
    var res = handle_basic_movement(delta, gmp)

    update_arms(gmp)

    update_player_animations(gmp)

    # Handle Build GameMode inputs
    var ll = get_parent().get_parent()
    match ll.game_mode:
        ll.GameMode.BUILD:
            handle_build_mode_inputs(delta, dir, gmp, ll)
        ll.GameMode.DEFEND, ll.GameMode.GET_LOOT:
            handle_defend_mode_inputs(delta, dir, gmp)

    handle_pickups()
    z_index = global.calculate_z_index(get_global_position())

func rotate_build_flags_left():
    match [build_vertical, build_flip]:
        [false, false]:
            build_vertical = true
            build_flip = false
        [true, false]:
            build_vertical = false
            build_flip = true
        [false, true]:
            build_vertical = true
            build_flip = true
        [true, true]:
            build_vertical = false
            build_flip = false

func rotate_build_flags_right():
    match [build_vertical, build_flip]:
        [false, false]:
            build_vertical = true
            build_flip = true
        [true, false]:
            build_vertical = false
            build_flip = false
        [false, true]:
            build_vertical = true
            build_flip = false
        [true, true]:
            build_vertical = false
            build_flip = true

func handle_pickups():
    for a in $PickupArea.get_overlapping_areas():
        var n = regex.sub(a.get_name(), "", true)
        if "AmmoPack" in n:
            var sp = current_gun.get_node("Sprite")
            if sp:
                sp.more_ammo()
            print("Received: Ammo")
            a.queue_free()
        if "HealthPack" in n and $health.health < 100:
            $health.health = min(100, $health.health * (health_improvement / 100.0 + 1.0))
            print("Received: Health")
            a.queue_free()
        if a.is_in_group("emplacement") and a.dropped and not n in global.player_emp_selection_names:
            print("Added contraption: " + n)
            global.player_emp_selection_names.append(n)
            global.load_available_emps()
            a.queue_free()
        if global.player_gun_selection_names.size() < 2 and OS.get_ticks_msec() - time_last_drop > 3000:
            if a.is_in_group("weapon") and not global.player_gun_selection_names.has(n):
                print("Added: " + n)
                global.player_gun_selection_names.append(n)
                global.load_available_guns()
                a.queue_free()
                time_last_drop = OS.get_ticks_msec()

func die():
    print("player died!")
    yield(get_tree().create_timer(1.0), "timeout")
    global.goto_scene("res://Scenes/GameOver.tscn")
