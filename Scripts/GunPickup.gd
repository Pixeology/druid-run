extends Area2D


func _process(delta):
    if $Sprite.dropped:
        for body in self.get_overlapping_bodies():
            if body.get_collision_layer_bit(0):
                print("Collided with player!")
                global.player_gun_selection.append(self.get_name())
                global.load_available_guns()