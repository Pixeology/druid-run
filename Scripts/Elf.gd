extends "res://Actors/Enemy.gd"

export(SpriteFrames) var heal_animation
export(int) var heal_amount
export(int) var heal_cooldown
onready var last_heal_time = OS.get_ticks_msec()

func _physics_process(delta):
    ._physics_process(delta)
    var obodies = $HealArea.get_overlapping_bodies()
    var bodies = []
    for b in obodies:
        if b.is_in_group("enemy") and b.get_node("health") and b.get_node("health").health < 100:
            bodies.append(b)
    if bodies.size() > 1 and OS.get_ticks_msec() - last_heal_time > heal_cooldown:
        _heal(bodies)

func _heal(bodies):
    $Sprite.play("Shooting")
    $health.health += heal_amount
    for b in bodies:
        if b.is_in_group("enemy") and b.get_node("health"):
            if heal_animation:
                var anim = global.animated_explosion.instance()
                anim.get_node("AnimatedSprite").set_sprite_frames(heal_animation)
                anim.set_global_position(b.get_global_position())
                anim.get_node("AnimatedSprite").play()
                get_parent().add_child(anim)
            b.get_node("health").health += heal_amount
    last_heal_time = OS.get_ticks_msec()
    $Sprite.play("default")