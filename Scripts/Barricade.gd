extends "res://Scripts/ContraptionBase.gd"

onready var maps = [get_parent().get_child(0).get_node("Dirt"), get_parent().get_child(0).get_node("Tiles")]
var texture
var prev_id
var dropped

func _ready():
    ._ready()
    if not dropped:
        chamelion()

func _process(delta):
    if get_child_count() == 0 and not dropped:
        chamelion()

func chamelion():
    for i in [0,1]:
        var spr = Sprite.new()
        var id = prev_id[i]
        var atlas = AtlasTexture.new()
        atlas.set_atlas(maps[i].tile_set.tile_get_texture(id))
        atlas.set_region(maps[i].tile_set.tile_get_region(id))
        spr.set_texture(atlas)
        spr.show_behind_parent = true
        spr.scale = Vector2(4,4)
        add_child(spr)
