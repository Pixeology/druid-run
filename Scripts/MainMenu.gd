extends Control

var scene_path_to_load

func _ready():
    for button in $Menu/CenterRow/Buttons.get_children():
        button.connect("pressed", self, "_on_Button_pressed", [button.scene_to_load])
        button.auto_change_scene = false

func _on_Button_pressed(scene_to_load):
    scene_path_to_load = scene_to_load
    $FadeIn.show()
    $FadeIn.fade_in()

func _on_FadeIn_fade_finished():
    var scene = load(scene_path_to_load).instance()
    get_parent().add_child(scene)
    get_parent().remove_child(self)
