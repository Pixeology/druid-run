extends Node2D

export(int) var player_level = 1

var player_gun_selection = []
var player_emp_selection = []
var player_gun_selection_names = ["Autoshotgun", "Spearerator"]
var player_emp_selection_names = []

var all_items = [
    [load("res://Scenes/Guns/LMGun.tscn"),
     load("res://Scenes/Guns/LaserGun.tscn"),
     load("res://Scenes/Contraptions/Crossbow.tscn"),
     load("res://Scenes/Contraptions/Spikes.tscn")],
    [load("res://Scenes/Guns/Cricketgun.tscn"),
     load("res://Scenes/Guns/Autoshotgun.tscn"),
     load("res://Scenes/Contraptions/Fan.tscn"),
     load("res://Scenes/Contraptions/Tripwire.tscn"),
     load("res://Scenes/Contraptions/TeleporterBlue.tscn"),
     load("res://Scenes/Contraptions/TeleporterRed.tscn")]
]

var new_scene
var current_scene
var player_emp_selection_limits
var gun_ammos = {}

onready var animated_explosion = load("res://Scenes/AnimatedExplosion.tscn")

func _ready():
    reload_default_guns()

func calculate_z_index(pos):
    return max(20, int(ceil(pos.y / 4.0)))

func all_items_for_level(l):
    return all_items[l - 1]

func reload_default_guns():
    player_emp_selection_names = [
        "Barricade",
        "Fan",
        "TeleporterRed",
        "TeleporterBlue",
        "ExplosiveBarrel"
    ]
    player_emp_selection_limits = {
        "Barricade": 3,
        "Crossbow": 10,
        "Fan": 10,
        "Tripwire": 3,
        "TeleporterRed": 1,
        "TeleporterBlue": 1,
        "Spikes": 10,
        "ExplosiveBarrel": 3
       }
    load_available_guns()
    load_available_emps()
    current_scene = get_tree().current_scene

func load_available_guns():
    player_gun_selection = []
    print("Reload: ")
    for c in player_gun_selection_names:
        var place = "res://Scenes/Guns/" + c + ".tscn"
        print("    " + place) # res://Scenes/Guns/Dagger.tscn
        var t = load(place)
        player_gun_selection.append(t)
        var sp = t.instance().get_node("Sprite")
        if sp:
            var ammo = sp.get("starting_ammo")
            if ammo:
                gun_ammos[c] = ammo
            else:
                gun_ammos[c] = INF
        else:
            gun_ammos[c] = INF

func load_available_emps():
    player_emp_selection = []
    print("Requip: ")
    for c in player_emp_selection_names:
        var place = "res://Scenes/Contraptions/" + c + ".tscn"
        print("    " + c)
        player_emp_selection.append(load(place))

func goto_scene(path):
    var scene = load(path).instance()
    var root = get_tree().get_root()
    for c in root.get_children():
        if c.get_name() != "global":
            root.remove_child(c)
    root.add_child(scene)
    Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)


func get_animation_and_direction(a, b):
    match [a, b]:
        [false, false]:
            return [0, Vector2(-1, 0)]
        [false, true]:
            return [1, Vector2(1, 0)]
        [true, false]:
            return [2, Vector2(0, -1)]
        [true, true]:
            return [3, Vector2(0, 1)]

func get_flip_flags_from_animation(a):
    match a:
        "Horizontal":
            return [false, false]
        "Horizontal_Right":
            return [false, true]
        "Vertical":
            return [true, false]
        "Vertical_Down":
            return [true, true]

func get_node_at_pos(pos):
    for node in get_tree().get_nodes_in_group("emplacement"):
        if node.get_global_position() == pos:
            return node
