extends AnimatedSprite

export(int) var laser_range
export(int) var camera_shake
export(int) var recoil
export(int) var laser_damage
export(int) var laser_push
export(int) var barrel_length
export(float) var laser_width

var player_ref
var dropped
var space_state
var ammo = INF
var ray_result
var shooting
var starting_ammo = INF

func _physics_process(delta):
    if not space_state:
        space_state = get_world_2d().direct_space_state

func _process(delta):
    if not player_ref:
        player_ref = get_tree().get_root().get_node("Loader/NavigationLayer/Player")

    var is_build_mode = $"/root/Loader".game_mode == $"/root/Loader".GameMode.BUILD

    var gmp = get_global_mouse_position()
    if not dropped and not is_build_mode:
        self.visible = true
        var start = player_ref.get_global_position()
        look_at(gmp)
        rotation_degrees += 45 * (-1 if flip_v else 1)
        if (Input.is_mouse_button_pressed(BUTTON_LEFT) or Input.is_key_pressed(KEY_SPACE))\
           and not self.is_playing()\
           and not player_ref.running:
            if not shooting:
                self.play("Fire")
                yield(self, "animation_finished")
                self.stop()
            shooting = true
        else:
            shooting = false


    if dropped or player_ref.running or is_build_mode:
        shooting = false

    if is_build_mode:
        self.visible = false

    var start = $"../LaserEmitPoint".get_global_position()
    if shooting and not dropped:
        player_ref.get_node("Camera2D").camera_shake(camera_shake, 0.3)
        var dir = (gmp - start).normalized()
        start += dir * barrel_length
        ray_result = space_state.intersect_ray(start, start + dir * laser_range * 4, [player_ref]) #?
        if not ray_result.empty() and ray_result.collider:
            if ray_result.collider.is_in_group("enemy"):
                ray_result.collider.get_node("health").get_hit(laser_damage)
                ray_result.collider.apply_impulse(dir * laser_push)
            elif ray_result.collider.is_in_group("destructable"):
                ray_result.collider.get_node("health").get_hit(laser_damage)

    update()

func more_ammo():
    pass

func _draw():
    if shooting:
        var start = $"../LaserEmitPoint".get_position() / 4
        var end = Vector2(laser_range, 0)
        if ray_result:
            var dist = (self.get_global_position() + start).distance_to(ray_result.position) - 20
            end = Vector2(dist / 4, 0)
        end = end.rotated(-deg2rad(45 * (-1 if flip_v else 1)))
        draw_line(start, end, Color(0, 0, 1, 0.8), laser_width, true)
        draw_line(start, end, Color(1, 1, 1, 0.5), laser_width / 2.0, true)
        draw_line(start, end, Color(1, 1, 1, 1), laser_width / 4.0, true)