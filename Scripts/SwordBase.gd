extends AnimatedSprite

export(int) var damage_per_slash
export(int) var carry_through_amount
export(bool) var use_combos
export(int) var combo_chain_length
export(float) var combo_boost
export(int) var final_combo_carry_through_amount
export(int) var combo_margin
export(bool) var should_attack_work
export(bool) var use_camera_shake

var dropped
var player_ref
var direction
var last_slash_time = OS.get_ticks_msec()
var combo_count = 0
var loader_ref
onready var animationplayer = $"../AnimationPlayer"
onready var anim = animationplayer.get_animation("Slash")

func _ready():
    animationplayer.connect("animation_finished", self, "animation_stopped")

func _process(delta):
    if not player_ref:
        player_ref = get_tree().get_root().get_node("Loader/NavigationLayer/Player")
    if not loader_ref:
        loader_ref = $"/root/Loader"

    var gmp = get_global_mouse_position()
    direction = (gmp - get_global_position()).normalized()

    if OS.get_ticks_msec() - last_slash_time >= 2*combo_margin+(anim.length * 1000.0):
        combo_count = 0

    var gm = loader_ref.game_mode
    if gm == loader_ref.GameMode.BUILD:
        self.visible = false
    if not dropped and gm != loader_ref.GameMode.BUILD:
        self.visible = true
        if Input.is_action_just_pressed("ui_build_emp") and not animationplayer.is_playing():
            slash()
        elif not animationplayer.is_playing():
            get_parent().look_at(gmp)

func more_ammo():
    damage_per_slash += 1

func slash():

    if use_combos:
        if OS.get_ticks_msec() - last_slash_time <= combo_margin+(anim.length * 1000.0) and combo_count < combo_chain_length:
            combo_count += 1
        else:
            combo_count = 0

        if combo_count < combo_chain_length:
            loader_ref.get_node("HUD/Message").show_message("Combo " + str(combo_count+1) + "!")
            player_ref.apply_impulse(direction * carry_through_amount)
        elif combo_count == combo_chain_length:
            loader_ref.get_node("HUD/Message").show_message("FINAL COMBO!")
            player_ref.apply_impulse(direction * final_combo_carry_through_amount)

        if use_camera_shake:
            player_ref.get_node("Camera2D").camera_shake(combo_count + 9, anim.length)

    animationplayer.play("Slash")
    last_slash_time = OS.get_ticks_msec()

func animation_stopped():
    pass

func _on_Sword_body_entered(body):
    if animationplayer.is_playing() and should_attack_work:
        var dmg = damage_per_slash * (pow(combo_boost + 1.0, combo_count) if use_combos else 1.0)
        if body.is_in_group("enemy") and not body.is_in_group("incorporial"):
            body.stun(0.1)
            body.get_node("health").get_hit(dmg)
            print("Damage: " + str(dmg))
        elif body.is_in_group("destructable"):
            body.get_node("health").get_hit(dmg)
