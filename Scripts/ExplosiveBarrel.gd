extends "res://Scripts/ContraptionBase.gd"

export(SpriteFrames) var explosion_animation
export(int) var damage
var dropped

func _physics_process(delta):
    update()

func stun(n):
    pass

func die():
    if not dropped:
        for b in $ExplosionArea.get_overlapping_bodies():
            if b != self and not b.is_in_group("explosive"):
                if b.get_node("health"):
                    if explosion_animation:
                        var anim = global.animated_explosion.instance()
                        anim.get_node("AnimatedSprite").set_sprite_frames(explosion_animation)
                        anim.set_global_position(b.get_global_position())
                        anim.get_node("AnimatedSprite").play()
                        get_parent().get_parent().get_parent().add_child(anim)
                    b.get_node("health").get_hit(damage)
                if b.is_in_group("destructable"):
                    b.destruct()
        queue_free()

func destruct():
    if not dropped:
        die()

func _draw():
    if not dropped:
        var percent = $health.health / 100.0
        var color = Color(0, 1, 0, 0.7)
        if percent < 0.3:
            color = Color(1, 0, 0, 0.9)
        elif percent <= 0.6:
            color = Color(200.0/255.0, 83.0/225.0, 0, 0.8)
        draw_line(Vector2(-16,-16), Vector2(20.0*percent,-16), color, 4.0)
