extends Button

export(String) var scene_to_load
export(bool) var auto_change_scene = true

func _on_pressed():
    print("Button clicked: " + scene_to_load)
    if auto_change_scene:
        get_tree().change_scene(scene_to_load)

func _on_MenuButton_mouse_entered():
    $ButtonLabel.add_color_override("font_color", Color(0.25,0.25,0.25))

func _on_MenuButton_mouse_exited():
    $ButtonLabel.add_color_override("font_color", Color(0,0,0))
