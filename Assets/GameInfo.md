# Game Notes

The goal of the game is to defend the bastion. enemies will come through different passages to try and break into the main room.
Each Player gets 4 barricades to block off the various halways, these barricades will get broken by all enemy attacks, some more
than others.  The barricades will repair to 100% hp after each Phase. There will be a chest phase after every set of waves, before
the loot drop team damage is turned on allowing players to duke it out for the best loot, players who die during this phase will
be given a choice of emplacements and weapon from the chest that the winner didnt take. Emplacemets will consist of auto turrets,
teleporters, tripwires,tesla coils and tons of other things. Maps will have natural hazards that the player can take advatage of
by using his own emplacements.

There will be a scoreboard, every kill players get including team kills will add to thier score, at the end of each level the player with highest score will be crowned and the other players will have to suffer through dumb humiliating insults. This will keep players playing together while against each other.

# Lists

## Guns

|Name 	     | Projectile Amount | Projectile Speed | Muzzle Spread | Accuracy | Shot Delay | Recoil | Camera Shake | Ammo | Ammo Per Pack |
|------------|-------------------|------------------|---------------|----------|------------|--------|--------------|------|---------------|
|LMG  	     | 1		 | 950		    | 0		    | 2	       | 80	    | 30     | 2	    | 15000| 1000 	   |
|Cricket Gun | 1		 | 950		    | 0		    | 15       | 1500	    | 1500   | 10	    | 100  | 3	 	   |
|AutoShotGun | 12		 | 300		    | 90	    | 13       | 500	    | 5      | 5	    | 1500 | 45 	   |

## Emplacements

### Spikes
Slow enemies and damage them over the period they are on the spikes

### Teleporter
Teleports player instantly between 2 points. Up to 2 pairs of teleporters are allowed at a time.

### Health Pack
Spawns a pack restoring hp to max for 1 player every 20 seconds.

### Ammo Pack
Spawns a pack restoring ammo to max for one player every 20 seconds.

### Explosive Barrel
Can be shot by the player to cause an explosion killing nearby enemies.

### 

###### enemies

## Skeleton
Uses a broadsword and has meduim health.

## Bomb Skeleton
Moves Quickly and detonates itself when near the players

## ELF
Uses a wand to heal nearby enemies, high hp and low speed.

## Blaze
A green fireball that shoots fireballs at the player. High hp and immune to physical damage.
