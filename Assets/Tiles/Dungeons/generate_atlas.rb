puts <<-'EOF'
Welcome to
________________ _______ _______ _______    _________________       _______ _______
\__   __(       |  ___  |  ____ (  ____ \  (  ___  )__   __( \     (  ___  |  ____ \
   ) (  | () () | (   ) | (    \/ (    \/  | (   ) |  ) (  | (     | (   ) | (    \/
   | |  | || || | (___) | |     | (__      | (___) |  | |  | |     | (___) | (_____
   | |  | |(_)| |  ___  | | ____|  __)     |  ___  |  | |  | |     |  ___  (_____  )
   | |  | |   | | (   ) | | \_  ) (        | (   ) |  | |  | |     | (   ) |     ) |
___) (__| )   ( | )   ( | (___) | (____/\  | )   ( |  | |  | (____/\ )   ( /\____) |
\_______//     \|/     \(_______|_______/  |/     \|  )_(  (_______//     \\_______)
v0.1

Run Image Atlas in the folder where you have all the images you want to include.
Image Atlas searches folders recursively, meaning that if you have a directory
structure with subdirectories that also contain images, like:

- Tiles/
  - Image.png
  - Image2.png
  - OtherTiles/
    - Image3.png
    - Image4.png

All of the images, even in subdirectories and subdirectories of _those_ subdir-
ectories will be included.

Please follow the prompts. Hit enter when you have typed in an answer.
The prompts are here to help.
EOF

print "What would you like to call the output image? "
$IMAGE_NAME = gets.chomp

File.delete($IMAGE_NAME) if File.exist?($IMAGE_NAME)

$WIDTH = 10
while true do
  print "How many columns of images should #{$IMAGE_NAME} have? "
  $WIDTH = gets.chomp.to_i

  print "Is #{$WIDTH} images OK (yes/no)? "
  yn = gets.chomp

  if yn.downcase.start_with? "y"
    puts "OK, continuing"
    break
  end
end

$FILES = ""

Dir.glob("**/*").sort.select { |file| file.end_with? ".png" }.each_slice($WIDTH) do |files|
  $FILES += "\\( " + files.join(" ") + " +append \\) "
end

`convert -background none #{$FILES} -append #{$IMAGE_NAME}`

#`convert -background none *.png +append atlas.png`
