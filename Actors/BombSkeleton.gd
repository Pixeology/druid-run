extends "res://Actors/Enemy.gd"

export(SpriteFrames) var explosion_animation
export(int) var damage

func _attack(event):
    for b in $ExplosionArea.get_overlapping_bodies():
        if b.is_in_group("player") and b.get_node("health"):
            if explosion_animation:
                var anim = global.animated_explosion.instance()
                anim.get_node("AnimatedSprite").set_sprite_frames(explosion_animation)
                anim.set_global_position(b.get_global_position())
                anim.get_node("AnimatedSprite").play()
                get_parent().add_child(anim)
            b.get_node("health").get_hit(damage)
        if b.is_in_group("destructable"):
            b.destruct()
    queue_free()