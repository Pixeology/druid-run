extends KinematicBody2D

export(int) var velocity

onready var current_speed = Vector2(0,0)
onready var path = []
onready var stop = false
onready var last_player_hit = OS.get_ticks_msec()
var in_progress_index
var player_ref
onready var stunned = false
onready var tilemap = get_parent().get_child(0).get_node("Tiles")
var dir
onready var goal_dist = 1

const P_OFFSET = Vector2(0,0)

func _ready():
    go_to_player()

func go_to_player():
    if not player_ref:
        player_ref = _select_player_ref()
    if player_ref:
        calculate_path_to(player_ref.get_global_position())

func _select_player_ref():
    var p = get_parent().get_node("Player")
    var b = get_parent().get_node("Bastion")
    if p and b:
        if p.get_global_position().distance_to(get_global_position()) > b.get_global_position().distance_to(get_global_position()):
            return p
        else:
            return b

func calculate_path_to(goal):
    path = get_parent().get_simple_path(get_global_position(), goal, false)
    in_progress_index = 0

func go_to_next_step(delta):
    if in_progress_index < path.size():
        dir = (path[in_progress_index] - get_global_position()).normalized()
        var event = move_and_collide(dir * velocity * delta)
        if event and event.collider.is_in_group("player"):
            self._attack(event)
        if dir.x < 0:
            $Sprite.flip_h = true
        else:
            $Sprite.flip_h = false
        if in_progress_index < path.size() - 1 and get_global_position().distance_to(path[in_progress_index]) < 1:
            in_progress_index += 1
            self._arrived_at_point()
        elif in_progress_index == path.size() - 1 and get_global_position().distance_to(path[in_progress_index]) < goal_dist:
            print("Final pnt")
            in_progress_index += 1
            self._arrived_at_point()
    else:
        go_to_player()

func _arrived_at_point():
    pass

func _attack(event):
    if OS.get_ticks_msec() - last_player_hit > 400:
        event.collider.get_node("health").get_hit(10)
        last_player_hit = OS.get_ticks_msec()
        event.collider.impulse((event.position - get_global_position()).normalized() * 900)

func stun(stun_time):
    var timer = Timer.new()
    add_child(timer)
    timer.connect("timeout", self, "unstun")
    timer.set_wait_time(stun_time)
    timer.start()
    stunned = true

func unstun():
    stunned = false

func _physics_process(delta):
    $CollisionShape2D.disabled = stop
    var pos = get_global_position()
    var map_pos = Vector2(floor(pos.x / (16*4)), floor(pos.y / (16*4)))
    var t = tilemap.get_cell(map_pos.x, map_pos.y)
    if t <= 39 and t >= 34:
        stop = true
        self.die("Death by Pit")
    if not stop:
        var event = move_and_collide(current_speed * delta)
        current_speed *= 0.9
        if not stunned:
            go_to_next_step(delta)
    update()
    z_index = global.calculate_z_index(get_global_position())

func _draw():
    if $health.health > 0:
        var percent = $health.health / 100.0
        var color = Color(0, 1, 0, 0.7)
        if percent < 0.3:
            color = Color(1, 0, 0, 0.9)
        elif percent <= 0.6:
            color = Color(200.0/255.0, 83.0/225.0, 0, 0.8)
        draw_line(Vector2(-16*2,-16*4), Vector2(16.0*2.0*percent,-16*4), color, 4.0)

func apply_impulse(i):
    if not stunned:
        current_speed = i

func die(animation="Death"):
    update()
    stop = true
    $Sprite.stop()
    if $Shadow:
        $Shadow.visible = false
    if animation:
        $AnimationPlayer.play(animation)
        if animation in $Sprite.frames.get_animation_names():
            $Sprite.play(animation)
        $Sprite.connect("animation_finished", self, "queue_free")
