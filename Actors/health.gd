extends Node

export(int) var health


func get_hit(damage):
    health = max(0, health - damage)
    if health > 0:
        if $"../BloodParticles":
            $"../BloodParticles".visible = true
            $"../BloodParticles".rotation = get_parent().get_position().angle_to(get_parent().current_speed) - 180
            $"../BloodParticles".play()
    if health <= 0:
        get_parent().die()
