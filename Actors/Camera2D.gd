extends Camera2D

    # Animate this to increase/decrease/fade the shaking
var shake_amount = 0.0
var decay_amount = -.0

func _process(delta):
    self.set_offset(Vector2( \
        rand_range(-1.0, 1.0) * shake_amount, \
        rand_range(-1.0, 1.0) * shake_amount \
    ))
    if shake_amount > 0:
        shake_amount -= decay_amount * delta
    else:
        shake_amount = 0

func camera_shake(amount, time):
    shake_amount = amount
    decay_amount = shake_amount / time