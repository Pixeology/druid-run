extends KinematicBody2D

onready var impulse = Vector2(0,0)

func _ready():
    z_index = global.calculate_z_index(get_global_position() - Vector2(0, 100))

func _physics_process(delta):
    var event = move_and_collide(impulse * delta)
    impulse *= 0.1

func apply_impulse(i):
    impulse = i

func die():
    yield(get_tree().create_timer(1.0), "timeout")
    global.goto_scene("res://Scenes/GameOverBastion.tscn")